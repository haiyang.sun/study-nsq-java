package com.test.nsq;

import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.NSQProducer;
import com.github.brainlag.nsq.exceptions.NSQException;
import com.github.brainlag.nsq.lookup.DefaultNSQLookup;
import com.github.brainlag.nsq.lookup.NSQLookup;
import org.apache.logging.log4j.LogManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.Executor;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NsqApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void sub(){
		NSQLookup lookup = new DefaultNSQLookup();
		lookup.addLookupAddress("localhost", 4161);
		NSQConsumer consumer = new NSQConsumer(lookup, "test", "dustin", (message) -> {
			System.out.println("received: " + new String(message.getMessage() ));
			System.out.println("received-id: " + new String(message.getId()));
			//now mark the message as finished.
			message.finished();

			//or you could requeue it, which indicates a failure and puts it back on the queue.
			//message.requeue();
		});


		consumer.start();

//线程睡眠，让程序执行完
		while (true){

		}

	}

	@Test
	public void pub(){
		try {
			NSQProducer producer = new NSQProducer().addAddress("localhost", 4150).start();

			producer.produce("test", ("this is a message").getBytes());
		}
		catch (NSQException e){
			System.out.println(e.getMessage());
		}
		catch (TimeoutException e){
			System.out.println(e.getMessage());
		}
	}


}
