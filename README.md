# study-nsq-java

#### 项目介绍
spring boot used nsq

#### 软件架构
软件架构说明


#### 安装教程

```

		<dependency>
			<groupId>com.github.brainlag</groupId>
			<artifactId>nsq-client</artifactId>
			<version>1.0.0.RC4</version>
```

#### 使用说明
```
	@Test
	public void sub(){
		NSQLookup lookup = new DefaultNSQLookup();
		lookup.addLookupAddress("localhost", 4161);
		NSQConsumer consumer = new NSQConsumer(lookup, "test", "dustin", (message) -> {
			System.out.println("received: " + new String(message.getMessage() ));
			System.out.println("received-id: " + new String(message.getId()));
			//now mark the message as finished.
			message.finished();

			//or you could requeue it, which indicates a failure and puts it back on the queue.
			//message.requeue();
		});


		consumer.start();

//线程睡眠，让程序执行完
		while (true){

		}

	}

	@Test
	public void pub(){
		try {
			NSQProducer producer = new NSQProducer().addAddress("localhost", 4150).start();

			producer.produce("test", ("this is a message").getBytes());
		}
		catch (NSQException e){
			System.out.println(e.getMessage());
		}
		catch (TimeoutException e){
			System.out.println(e.getMessage());
		}
	}
``` 